var express = require('express');
var methodOverride = require('method-override');

var app = express();
var bodyParser = require('body-parser');



app.use(express.static(__dirname + '/public'));

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");  //maybe change this to our url?
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    next();
});

require('./routes')(app);

app.get('/', function(req, res, next) {
    if(!req.secure) 
    {
        res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    else
    {
        res.render('index', {
            title: 'Facemelt'
        });
    }
    next();
});



app.listen(process.env.PORT || 3000, function () {
  console.log('Express server listening on port ' + 3000);
});
