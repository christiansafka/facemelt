var Facemelt = angular.module('Facemelt', ['angularjs-datetime-picker', 'rzModule']);


Facemelt.controller('mainController', ['$scope', '$location', '$window', '$anchorScroll',
	function($scope, $location, $window, $anchorScroll) {

		window.markerArray = [];
		$scope.name = "Facemelt";
		$scope.origin = "Delran";
		$scope.destination = "Moorestown";

		$scope.$watch(function(){
			return $location.path();
		}, function(value){
			console.log(value);
			console.log(value.charAt(1));
			if(value.charAt(1) == '#')
			{
				$scope.origin = value.split('from=')[1].split('&')[0];
		    	$scope.destination = value.split('to=')[1];  //add error handling
		    }
		    else
		    {
		    	//$location.hash('about');
		    	//$anchorScroll();
		    }

		});

		  $scope.$watch(function(){
  		return $scope.slider.value;
	  }, function(value){
	  	console.log(value);

	  });

	  $scope.$watch(function(){
	  	return $scope.origin;
	  }, function(value){
	  	console.log(value);
	  	calculateAndDisplayRoute(
	  		$scope.directionsDisplay,  $scope.directionsService,  $scope.markerArray,  $scope.stepDisplay
	  		,  $scope.map,  $scope.indexArray,  $scope.startArray);
	  });



		$scope.slider = {
			value: 0,
			options: {
				floor: 0,
				ceil: 100
			}
		};







		function calculateAndDisplayRoute(directionsDisplay, directionsService,
			markerArray, stepDisplay, map, indexArray, startArray) {
			console.log('called here', arguments);
                                    // First, remove any existing markers from the map.
                                    for (var i = 0; i < markerArray.length; i++) {
                                    	if(markerArray[i] != null){
                                    		markerArray[i].setMap(null);
                                    	}
                                    }
                                    indexArray.length=0;
                                    startArray.length=0;

                                    // Retrieve the start and end locations and create a DirectionsRequest using
                                    // WALKING directions.
                                    
                                    directionsService.route({
                                    	origin: $scope.origin,
                                    	destination: $scope.destination,
                                    	travelMode: google.maps.TravelMode.DRIVING
                                    }, function (response, status) {
                                        // Route the directions and pass the response to a function to create
                                        // markers for each step.
                                        if (status === google.maps.DirectionsStatus.OK) {
                                            //document.getElementById('warnings-panel').innerHTML =
                                             //       '<b>' + response.routes[0].warnings + '</b>';
                                             directionsDisplay.setDirections(response);
                                             showOverview(response, markerArray, stepDisplay, map,indexArray,startArray);
                                         } else {
                                         	window.alert('Directions request failed due to ' + status);
                                         }
                                     });

                                }
                                function showOverview(directionResult, markerArray, stepDisplay, map,indexArray, startArray) {
                                	var overView = directionResult.routes[0].overview_path;
                                	var start = overView[0];
                                	var nrOfArray = $scope.slider.value;

                                	for (var i = 1; i < overView.length; i++) {
                                		var endOfLng = overView[i];
                                		var distance = google.maps.geometry.spherical.computeDistanceBetween(start, endOfLng);
                                		if(distance>=400) {
                                			var marker = markerArray[i - 1] = markerArray [i - 1] || new google.maps.Marker;
                                			indexArray[indexArray.length] = i - 1;
                                			startArray[startArray.length] = start;
                                			var nrOfSpots = overView.length;
                                		}




                                		if(i-1 == nrOfArray){
                                			marker.setMap(map);
                                			marker.setPosition(start);
                                		}

                                		start = endOfLng;
                                	}
                                	var carMarker = markerArray[indexArray[nrOfArray]];
                                	drawCarMarker(carMarker,map,startArray[nrOfArray]);
                                    //document.getElementById("katsetus").innerHTML = indexArray.length;
                                }
                                function drawCarMarker(markerOfTheCar, map, startpoint){
                                	markerOfTheCar.setMap(map);
                                	markerOfTheCar.setPosition(startpoint);

                                }



                            }]).directive('googleMap', ['$window', function ($window) {
                            	return {
                            		restrict: 'A',
                            		link: function (scope, element, attrs) {
                // If Google maps is already present then just initialise my map
                if ($window.google && $window.google.maps) {
                	initGoogleMaps();
                } else {
                	loadGoogleMapsAsync();
                }

                function loadGoogleMapsAsync() {
                    // loadGoogleMaps() == jQuery function from https://gist.github.com/gbakernet/828536
                    $.when(loadGoogleMaps())
                        // When Google maps is loaded, add InfoBox - this is optional
                        .done(function () {
                        	initGoogleMaps();
                        });
                    };

                    function initGoogleMaps() {


                    	scope.$apply(function(){

                    		$scope.styles = [
                    		{
                    			stylers: [
                    			{ hue: "#00ffe6" },
                    			{ saturation: -20 }
                    			]
                    		},{
                    			featureType: "road",
                    			elementType: "geometry",
                    			stylers: [
                    			{ lightness: 100 },
                    			{ visibility: "simplified" }
                    			]
                    		},{
                    			featureType: "road",
                    			elementType: "labels",
                    			stylers: [
                    			{ visibility: "off" }
                    			]
                    		}
                    		];


                    		var styledMap = new google.maps.StyledMapType(styles,
							  	{name: "map"});
							  
							 $scope.indexArray=[];
							  $scope.startArray=[];


							  var result = document.getElementById("map");
							  var themap = angular.element(result);

							    // Instantiate a directions service.
							    $scope.directionsService = new google.maps.DirectionsService;

							    console.log(1);
							  // to the map type control.
							  var mapOptions = {
							  	zoom: 11,
							  	center: new google.maps.LatLng(55.6468, 37.581),
							  	mapTypeControlOptions: {
							  		mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
							  	}
							  };
							  $scope.map = new google.maps.Map(themap,
							  	mapOptions);
							  console.log(2);
							  //Associate the styled map with the MapTypeId and set it to display.
							  map.mapTypes.set('map_style', styledMap);
							  map.setMapTypeId('map_style');
							  console.log(3);
							  $scope.directionsDisplay = new google.maps.DirectionsRenderer({map: map});

							  console.log("ff");
							  calculateAndDisplayRoute(
							  	$scope.directionsDisplay,  $scope.directionsService,  $scope.markerArray, $scope.stepDisplay
							  	,  $scope.map,  $scope.indexArray,  $scope.startArray);



							                    	});
                    	

  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
 
                    // Remember to wrap scope variables inside `scope.$apply(function(){...});`
                }
            }
        };
    }]);