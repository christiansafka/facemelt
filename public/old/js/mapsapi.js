

Facemelt.controller('mapController', ['$scope', '$location', '$element',
    function($scope, $location, $element) {
        $scope.name = "working";
        $scope.origin = "Delran";
        $scope.destination = "Moorestown";

        $scope.slider = {
            value: 0,
            options: {
                floor: 0,
                ceil: 100
            }
        };

                    var styles = [
                                    {
                                      stylers: [
                                        { hue: "#00ffe6" },
                                        { saturation: -20 }
                                      ]
                                    },{
                                      featureType: "road",
                                      elementType: "geometry",
                                      stylers: [
                                        { lightness: 100 },
                                        { visibility: "simplified" }
                                      ]
                                    },{
                                      featureType: "road",
                                      elementType: "labels",
                                      stylers: [
                                        { visibility: "off" }
                                      ]
                                    }
                                  ];

                                  // Create a new StyledMapType object, passing it the array of styles,
                                  // as well as the name to be displayed on the map type control.
                                  var styledMap = new google.maps.StyledMapType(styles,
                                    {name: "map"});
                                  var markerArray = [];
                                    var indexArray=[];
                                    var startArray=[];
                                    var index = $scope.slider.value;

                                    // Instantiate a directions service.
                                    var directionsService = new google.maps.DirectionsService;

                                  // Create a map object, and include the MapTypeId to add
                                  // to the map type control.
                                  var mapOptions = {
                                    zoom: 11,
                                    center: new google.maps.LatLng(55.6468, 37.581),
                                    mapTypeControlOptions: {
                                      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                                    }
                                  };
                                  var map = new google.maps.Map($('#map')[0],
                                    mapOptions);

                                  //Associate the styled map with the MapTypeId and set it to display.
                                  map.mapTypes.set('map_style', styledMap);
                                  map.setMapTypeId('map_style');

                                     var directionsDisplay = new google.maps.DirectionsRenderer({map: map});

                                    // Instantiate an info window to hold step text.
                                    var stepDisplay = new google.maps.InfoWindow;

                                    // Display the route between the initial start and end selections.
                                    calculateAndDisplayRoute(
                                            directionsDisplay, directionsService, markerArray, stepDisplay, map, indexArray, startArray);
                                    // Listen to change events from the start and end lists.
                                    var onChangeHandler = function () {
                                        calculateAndDisplayRoute(
                                                directionsDisplay, directionsService, markerArray, stepDisplay, map, indexArray, startArray);
                                    };

                                    $scope.$watch(function(){
                                        return $scope.slider.value;
                                    }, function(value){
                                        console.log(value);
                                                 
                                    });
                                    
                                    $scope.$watchGroup([$scope.origin, $scope.destination], function(newValues, oldValues, scope) {
                                         calculateAndDisplayRoute(directionsDisplay, directionsService,
                                                                  markerArray, stepDisplay, map, indexArray, startArray);
                                         console.log(newValues);
                                    });

                                function calculateAndDisplayRoute(directionsDisplay, directionsService,
                                                                  markerArray, stepDisplay, map, indexArray, startArray) {
                                    // First, remove any existing markers from the map.
                                    for (var i = 0; i < markerArray.length; i++) {
                                        if(markerArray[i] != null){
                                            markerArray[i].setMap(null);
                                        }
                                    }
                                   indexArray.length=0;
                                    startArray.length=0;

                                    // Retrieve the start and end locations and create a DirectionsRequest using
                                    // WALKING directions.
                                    
                                    directionsService.route({
                                        origin: $scope.origin,
                                        destination: $scope.destination,
                                        travelMode: google.maps.TravelMode.DRIVING
                                    }, function (response, status) {
                                        // Route the directions and pass the response to a function to create
                                        // markers for each step.
                                        if (status === google.maps.DirectionsStatus.OK) {
                                            //document.getElementById('warnings-panel').innerHTML =
                                             //       '<b>' + response.routes[0].warnings + '</b>';
                                            directionsDisplay.setDirections(response);
                                            showOverview(response, markerArray, stepDisplay, map,indexArray,startArray);
                                        } else {
                                            window.alert('Directions request failed due to ' + status);
                                        }
                                    });
                                
                                }
                                function showOverview(directionResult, markerArray, stepDisplay, map,indexArray, startArray) {
                                    var overView = directionResult.routes[0].overview_path;
                                    var start = overView[0];
                                    var nrOfArray = $scope.slider.value;

                                    for (var i = 1; i < overView.length; i++) {
                                        var endOfLng = overView[i];
                                        var distance = google.maps.geometry.spherical.computeDistanceBetween(start, endOfLng);
                                        if(distance>=400) {
                                            var marker = markerArray[i - 1] = markerArray [i - 1] || new google.maps.Marker;
                                            indexArray[indexArray.length] = i - 1;
                                            startArray[startArray.length] = start;
                                            var nrOfSpots = overView.length;
                                        }




                                       /* if(i-1 == nrOfArray){
                                            marker.setMap(map);
                                            marker.setPosition(start);
                                       }*/

                                       start = endOfLng;
                                    }
                                    var carMarker = markerArray[indexArray[nrOfArray]];
                                    drawCarMarker(carMarker,map,startArray[nrOfArray]);
                                    //document.getElementById("katsetus").innerHTML = indexArray.length;
                                }
                                function drawCarMarker(markerOfTheCar, map, startpoint){
                                    markerOfTheCar.setMap(map);
                                    markerOfTheCar.setPosition(startpoint);
                                
                                }

}]);

